from django.shortcuts import render, redirect
from .models import Account
from .forms import AccountForm

# Create your views here.
def index(request):
    if request.method == 'POST':
        form = AccountForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('homepage:index')
    else:
        form = AccountForm()        
    return render(request,'login/login.html',{
        'form' : form
    })