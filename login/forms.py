from django import forms
from.models import Account

class AccountForm(forms.ModelForm):
    name = forms.CharField(label='')
    name.widget.attrs.update({'class':'login-form', 'required':'required'})
    class Meta:
        model = Account
        fields =('name',)