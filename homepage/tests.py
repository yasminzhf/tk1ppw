from django.test import TestCase, Client
from django.urls import reverse, resolve
from .views import *
from .urls import *

# Create your tests here.
class HomepageTest(TestCase):
    def test_homepage_url_is_exist(self):
        response = Client().get('/home/')
        self.assertEqual(response.status_code,200)

    def test_homepage_is_using_homepage_template(self):
        response = Client().get('/home/')
        self.assertTemplateUsed(response, 'homepage/homepage.html')

    def test_homepage_is_using_index_func(self):
        found = resolve('/home/')
        self.assertEqual(found.func, index)