from django.apps import AppConfig


class StudfriendConfig(AppConfig):
    name = 'studfriend'
