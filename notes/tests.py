from django.test import TestCase, Client
from django.urls import resolve, reverse
from .models import Catatan
from .forms import CatatanForms

# Create your tests here.

class TestNotes(TestCase):

    def test_note_url(self):
        response = self.client.get('/catatan/')
        self.assertEquals(response.status_code, 200)

    def test_note_input(self):
        response = self.client.get('/catatan/')
        html_cek = response.content.decode('utf8')
        self.assertIn("Notes", html_cek)


    def test_note_template(self):
        response = self.client.get('/catatan/')
        self.assertTemplateUsed(response, 'notes/notes.html')

    def test_notes_model(self):
        cek_notes=Catatan.objects.create(judulcatatan='Test Catatan')
        catat = Catatan.objects.get(id=1)
        self.assertEqual(str(catat), 'Test Catatan')


    def test_save_note_a_POST_request(self):
        cek_notes=Catatan.objects.create(judulcatatan='Test Catatan')
        jmlh = Catatan.objects.all().count()
        self.assertEquals(jmlh, 1)
