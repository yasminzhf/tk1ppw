from django.db import models

# Create your models here.
class Catatan(models.Model):
    judulcatatan = models.CharField(max_length=100)
    isicatatan = models.CharField(max_length=500)
    
    def __str__(self):
        return self.judulcatatan