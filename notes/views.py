from django.shortcuts import render,redirect
from django.http import HttpResponse
from .models import Catatan
from .forms import CatatanForms


def tambah(request):  
    notes= Catatan.objects.all() 
    form = CatatanForms()
    if request.method == "POST":  
        form = CatatanForms(request.POST)  
        if form.is_valid():  
            try:  
                form.save()  
                return redirect('/catatan')  
            except:  
                pass  
    else:  
        form = CatatanForms()

    response = {
        'form': form,
        'notes' : notes,
    }
    return render(request,'notes/notes.html', response)  

def isi_catatan(request, judul_catatan):
    notes = Catatan.objects.filter(judulcatatan=judul_catatan)
    response = {
        'notes': notes,
    }
    return render(request, 'notes/isicatatan.html', response)

def hapus(request, judul_catatan):  
    notes = Catatan.objects.filter(judulcatatan=judulcatatan)  
    notes.delete()  
    return redirect("/catatan")  
