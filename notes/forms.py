from django import forms
from .models import Catatan

class CatatanForms(forms.ModelForm):
    judulcatatan=forms.CharField(label='')
    judulcatatan.widget.attrs.update({'class':'notes-form', 'required':'required'})
    isicatatan = forms.CharField(label='')
    isicatatan.widget.attrs.update({'class':'notes-form', 'required':'required'})

    class Meta:
        model = Catatan
        fields = ['judulcatatan','isicatatan']
    
