from django.contrib import admin
from django.urls import path, include
from . import views

app_name = 'notes'

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.tambah, name="index"),
    path('<str:nama_matkul>', views.isi_catatan),
    path('delete/<str:nama_matkul>', views.hapus),  
    
]