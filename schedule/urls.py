from django.contrib import admin
from django.urls import path, include
from . import views

app_name = 'schedule'

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.add, name="index"),
    path('<str:nama_matkul>', views.detail_jadwal),
    path('edit/<int:id>', views.edit),  
    path('update/<int:id>', views.update),  
    path('delete/<str:nama_matkul>', views.destroy),  
    
]