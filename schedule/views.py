from django.shortcuts import render,redirect
from django.http import HttpResponse
from .models import Jadwal
from .forms import JadwalForms




# Create your views here.
# def index(request):
#     return render(request,'schedule/schedule.html')

def add(request):  
    data = Jadwal.objects.all() 
    form = JadwalForms()
    if request.method == "POST":  
        form = JadwalForms(request.POST)  
        if form.is_valid():  
            try:  
                form.save()  
                return redirect('/jadwalmu')  
            except:  
                pass  
    else:  
        form = JadwalForms()

    response = {
        'form': form,
        'data' : data,
        
    }  
    #print(response)
    print("masuk")
    return render(request,'schedule/schedule.html', response)  

def detail_jadwal(request, nama_matkul):
    data = Jadwal.objects.filter(jadwalpelajaran=nama_matkul)
    response = {
        'data': data,
    }
    return render(request, 'schedule/listJadwal.html', response)



def destroy(request, nama_matkul):  
    data = Jadwal.objects.filter(jadwalpelajaran=nama_matkul)  
    data.delete()  
    return redirect("/jadwalmu")  

def edit(request, id):  
    data = Jadwal.objects.get(id=id)  
    return render(request,'schedule/edit.html', {'data': data})  


def update(request, id):  
    data = Jadwal.objects.get(id=id)  
    form = JadwalForms(request.POST, instance = data)  
    if form.is_valid():  
        form.save()  
        return redirect("/jadwalmu")  
    else:
        #form.save()
        return redirect("/jadwalmu")
    return render(request, 'schedule/edit.html', {'data': data})  
